import "./style.css"
import React from "react";
import { FaGlobe , FaSearch} from 'react-icons/fa';
import Ring from "../../../assets/images/svg/Ring";
import Image_Man from "../../../assets/images/001-man.svg"
import Menu from "../../../assets/images/svg/Menu";
import { useSelector , useDispatch } from "react-redux";
import {IState} from "../Left-Bar/types"
import { changePar } from "../../../redux/reducers/menuReducer";
const Top_bar:React.FC = () => {
    const click = useSelector((state: IState)=>state.click.click)
    const dispatch = useDispatch()
    
    
    return <div className="block flex">
        <div className="flex">
            <button onClick={()=>{
                dispatch(changePar(!click))
            }} className="button menu"><Menu/></button>
        </div>
        <div className="flex right_block">
            
            <div >
                <button className="ring"><Ring/></button>
            </div>
            <div>
                <img src={Image_Man} alt="" className="image_man" />
            </div>
        </div>

    </div>
    
}

export default Top_bar