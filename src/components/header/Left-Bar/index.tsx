import "./style.css"
import React, { useEffect } from "react";
import Logo from "../../../assets/images/Logo/logo.svg"
import Avatar from '@mui/material/Avatar';
import { Link } from "react-router-dom";
import Products from "../../../assets/images/svg/Products";
import { useSelector , useDispatch } from "react-redux";
import {IState } from "../Left-Bar/types"
import { changePar } from "../../../redux/reducers/menuReducer";
const Left_Bar:React.FC = () =>{

   const dispatch = useDispatch()
    const mobileHook = useSelector((state: IState)=>state.click.click)
    
  
      
    
    return <div className={`header_body ${mobileHook === false ? "aaa" : "zzz"}`}>
        <div className="logo_block">
        <div>
            <img src={Logo}/>
        </div>
        </div>
        <div className="left_bar">
            <p>ADMIN</p>
            <div>
                <Link onClick={() => { dispatch(changePar(!mobileHook)) } } to={"/product-list"} className="link "><Products/>Products</Link>
                <ul>
                <Link onClick={() => { dispatch(changePar(!mobileHook)) } }  to={"/product-list"} className="link"><li>Product List</li></Link>
                <Link onClick={() => { dispatch(changePar(!mobileHook)) } }  to={"/create-product"} className="link"><li>Create Product</li></Link>
                <Link onClick={() => { dispatch(changePar(!mobileHook)) } }  to={"/category"} className="link"><li>Category</li></Link>
                <Link onClick={() => { dispatch(changePar(!mobileHook)) } }  to={"/brands"} className="link"><li>Brands</li></Link>
                </ul>
            </div>
        </div>
    </div>
}


export default Left_Bar