import React, { useMemo } from 'react';
import { Pagination } from '@mui/material';
interface ICount {
  itemsPerPage?:number,
  onChange?:()=>void
  data?:{},
  total?:any
  isLoading?:boolean
}

const PaginatedItems = ( itemsPerPage:ICount ) => {
  
  const pagination_count:number = useMemo(()=>{
		if(!itemsPerPage.isLoading && itemsPerPage.data){
			return Math.ceil(itemsPerPage?.total / 5)
      
		}else{
			return 1
		}
		
	},[itemsPerPage.data,itemsPerPage.isLoading])
 
  
  return (
    <>
      <Pagination count={pagination_count} onChange={itemsPerPage.onChange}/>
    </>
  );
}

export default PaginatedItems