import { IBrands } from "../../redux/types/brands"

export interface IInput{
    type?: string,
    id?: string,
    placeholder?:string,
    required?:any,
    option?:IOption,
    onChange?: () => void,
    onBlur?:()=>void,
    value?: string | number,
    className?:string,
    touched?:boolean,
    errors?:string,
    accept?:string,
    image?:string,
    text?:string,
    loading?:boolean
}

export interface IOption {
    id?: number, 
    name?: string,
    value?:number
}