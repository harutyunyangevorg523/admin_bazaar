import React from "react";
import { IInput } from "../type";

const Textarea:React.FC<IInput> = (props:IInput) => {
    return <>
        <textarea className={`form-control resize ${props.className}`} onChange={props.onChange}
         onBlur={props.onBlur} placeholder={props.placeholder} id={props.id}
         required={props.required} value={props.value}
         ></textarea>
    <div id={props.id} className="invalid-feedback">
       {props.required}
      </div>
      
    </>
}

export default Textarea