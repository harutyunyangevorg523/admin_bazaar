import React from "react";
import { IInput , IOption} from "../type";

const Option:React.FC<IInput> = (props: IInput) => {
    const data:any = props.option   
   
   
    
    return <>
        <select  className={`form-select`} value={props.value} onChange={props.onChange}  id={props.id} >
            
                {
                data?.map((item:IOption)=>{     
                           
                    return <option key={item.id} value={item.id} >{item.name}</option>
                })
                }
                
              </select>
              <div id={props.id} className="invalid-feedback">
       {props.required}
      </div>
    </>
}

export default Option