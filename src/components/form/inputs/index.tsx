import { height } from "@mui/system";
import React from "react";
import {IInput} from "../type"

const Inputs:React.FC<IInput> = (props:IInput)=>{
    return <><input type={props.type} className={`form-control ${props.className}`} name={props.id} id={props.id}  placeholder={props.placeholder} 
     onChange={props.onChange} onBlur={props.onBlur}  value={props?.value} accept={props.accept}
    />
    
      {
        props.loading ? <img src={props.image} style={{height:"70px"}}  /> : ""
        
      }

      
    
    
    <span>{props.text}</span>
    <div id={props.id} className="invalid-feedback">
       {props.required}
      </div>
    </>
}


export default Inputs
