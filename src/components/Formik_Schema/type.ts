export interface ICreateFormik{
    name?:string,
    description?:string,
    price?:number,
    stock?:number,
    image?: string,
    brandId?:number,
    categoriesId?:number
}

