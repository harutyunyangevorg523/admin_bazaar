import * as Yup from 'yup';

export const product_schema = Yup.object().shape({
    name: Yup.string()
      .min(2, "Too Short!")
      .max(30, "Too Long!")
      .required("Name is required"),
    description: Yup.string()
      .min(10, "Too Short!")
      .max(2000, "Too Long!")
      .required("Description is required"),
      image: Yup.string()
      .required("Image is required"),
      price: Yup.number()
      .min(1, "Too Short!")
      .max(2000, "Too Long!")
      .required("price is required"),
      stock: Yup.number()
      .min(1, "Too Short!")
      .max(2000, "Too Long!")
      .required("Stock is required"),
      brandId: Yup.string()
      .required("Brand is required"),
      categoriesId:Yup.string()
      .required("categories is required"),
  
    
  });