import { number, string } from "yup"

export interface ICreateForm{
    text?:string,
    schema?:{},
    add?:any,
    onSubmit:() =>void,
    button?: string,
    initialValues?:initialValues,
    brand_data?:{},
    category_data?:{},
    oneProduct?:any,
    isLoading?:boolean,
    editProduct?:any,
    id?:number,
    loading?:boolean
}

type initialValues = {[key: string]: string | number | undefined}