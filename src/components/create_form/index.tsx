import React,{useMemo,useEffect} from "react";

import Inputs from "../../components/form/inputs";

import Textarea from "../../components/form/textarea";
import Option from "../../components/form/option";
import { useFormik, FormikProps } from "formik"
import { ICreateFormik } from "../../components/Formik_Schema/type";
import { ICreateForm } from "./type";
import { useLocation } from "react-router-dom";


const CreateForm:React.FC<ICreateForm> = (props: ICreateForm) => {
  const {pathname} = useLocation()
 
 const id =props.id
  const formik: FormikProps<ICreateFormik>  = useFormik<ICreateFormik>({
    initialValues: {
        ...props.initialValues
    },
  
    validationSchema:props.schema,
   
    onSubmit: (values: ICreateFormik ) =>{
        const ids:number[] = []
        const catId: any = values.categoriesId
        ids.push( +(catId))
        delete values.categoriesId 
      if(pathname == "/create-product"){
        props.add({
            ...values,
            categoriesIds: ids
          })
      }else if(pathname == `/update-product/${id}`){
        props.editProduct({
            id,
            payload: {
             ...values,
             categoriesIds: ids
            }
         })
      
    }else{
        props.add({
            ...values
          })
      }        
    },
    
  })
  
  
  const getBase64 = (event: React.ChangeEvent<any>) => {
    let file = event.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      formik.setValues({
        ...formik.values,
        image: String(reader?.result || '')
      })
      return reader.result
    };
  
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
  const oneProduct = props.oneProduct
  useEffect(()=>{
    if(!props.isLoading && oneProduct){
     
     formik.setValues({
         name: oneProduct?.name,
         description: oneProduct?.description,
         image: oneProduct?.image,
         price: oneProduct?.price,
         stock: oneProduct?.stock,
         brandId:oneProduct.brand?.id,
         categoriesId:oneProduct?.categories?.[0]?.id
     })
    }
    
 },[oneProduct,props.isLoading])
  const memo = useMemo(()=>{
    if(pathname == "/create-product" || pathname == `/update-product/${id}`){
        return <>
            <div className="col-sm-6">
           <Inputs type={"text"} id={"name"} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'is-valid'}`} 
              placeholder={"Name"} required={formik.errors.name} onChange={formik.handleChange as () =>void}
              touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void} value={formik.values.name} />
              
            </div>
            <div className="col-sm-6">
              <Option id={"categoriesId"} className={`${formik.touched.categoriesId && formik.errors.categoriesId ? 'is-invalid' : 'is-valid'}`}  onChange={formik.handleChange as ()=>void} 
              option={props.category_data} required={formik.errors.categoriesId}  value={formik.values.categoriesId}/>
          
            </div>
            <div className="col-sm-6">
              <Option id={"brandId"} option={props.brand_data}  value={formik.values.brandId} onChange={formik.handleChange as ()=>void}  required={formik.errors.brandId} className={`${formik.touched.brandId && formik.errors.brandId ? 'is-invalid' : 'is-valid'}`}/>
          
            </div>
            <div className="mb-3 mt-3" onChange={getBase64}>
              <Inputs type={"file"} id={"image"} image={formik.values.image} loading={props.loading}  required={formik.errors.image} onChange={formik.handleChange as () =>void} className={`${formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}`}  accept=".jpeg,.png,.jpg" />
              
            </div>
            <div className="mb-3 mt-3">
            <Textarea id={"description"} placeholder={"Description"}
            className={` block w-full rounded border py-1 px-2 ${formik.touched.description && formik.errors.description ? 'is-invalid' : 'is-valid'}`}
             required={formik.errors.description} value={formik?.values?.description} onChange={formik.handleChange as () =>void} onBlur={formik.handleBlur as ()=> void}/>         
            
          
            </div>
            <div className="col-sm-6">
            <Inputs type={"number"} id={"price"} className={`block w-full rounded border py-1 px-2 ${formik.touched.price && formik.errors.price ? 'is-invalid' : 'is-valid'}`} 
              placeholder={"Price"} required={formik.errors.price} onChange={formik.handleChange as () =>void}
              touched={formik.touched.price} errors={formik.errors.price} onBlur={formik.handleBlur as ()=> void} value={formik.values.price} 
              
              />     
            </div>
            <div className="col-sm-6">
            <Inputs type={"number"} id={"stock"} className={`block w-full rounded border py-1 px-2 ${formik.touched.stock && formik.errors.stock ? 'is-invalid' : 'is-valid'}`} 
              placeholder={"Stock"} required={formik.errors.stock} onChange={formik.handleChange as () =>void}
              touched={formik.touched.stock} errors={formik.errors.stock} onBlur={formik.handleBlur as ()=> void} value={formik.values.stock}               
              /> 
            </div>
        </>
    }else if(pathname == '/create-category'){
        return <div>
        <Inputs type={"text"} id={"name"} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'is-valid'}`} 
             placeholder={"Name"} required={formik.errors.name} onChange={formik.handleChange as () =>void}
             touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void} value={formik.values.name} />
          
        </div>
    }else{
       return <>
        <div>
        <Inputs type={"text"} id={"name"} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'is-valid'}`} 
             placeholder={"Name"} required={formik.errors.name} onChange={formik.handleChange as () =>void}
             touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void} value={formik.values.name} />
          
        </div>
         <div className="mb-3 mt-3" onChange={getBase64}>
           <Inputs type={"file"} id={"image"} required={formik.errors.image} onBlur={formik.handleBlur as ()=> void} onChange={formik.handleChange as () =>void} className={`${formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}`} accept=".jpeg,.png,.jpg" />
           
         
         </div>
         </>
    }
 },[formik,getBase64])
    return <div className="cr_product container">
    <div>
     <h3 className="title">{props.text}</h3>
     <div className="new_product">
    <form className="needs-validation">
       <div className="row g-3">
         {            
         memo
         }
         <div className="col-12">
         <button className="btn btn-primary btn-lg btn-block" onClick={formik.handleSubmit as () => void} type="button">{props.button}</button>
         </div>        
       </div>              
     </form>
     </div>
     </div>
 </div>
}

export default CreateForm