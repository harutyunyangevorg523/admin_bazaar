import React from "react";
import { Spinner } from "react-bootstrap";

const Loading:React.FC = () => {

    return <tr>
    <td><Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
  </Spinner></td>
</tr> 
}

export default Loading