import React from "react";
import { Link } from "react-router-dom";
import {IButton} from "./type"

const Button:React.FC<IButton> = (props:IButton)=>{
    return <div className="flex" style={{position: "relative"}}>
        <h4>{props.head}</h4>
        <button type="button" className="btn btn-primary" style={{position:"absolute",right:"0"}}><Link to={props.link} className="link">{props.text}</Link></button>
    </div>
    
}

export default Button