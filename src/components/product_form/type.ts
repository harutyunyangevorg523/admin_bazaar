export interface IForm{
    data?:{},
    date?:{},
    isLoading?:boolean,
    total?:number,
    onChange?:()=>void,
    thead:string[],
    delete?:any,
    head?:string,
    link:string,
    text:string
} 