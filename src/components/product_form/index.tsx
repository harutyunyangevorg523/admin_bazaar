import React from "react"; 
import { useLocation } from "react-router-dom";
import Button from "../../components/button";
import Loading from "../../components/loading";
import PaginatedItems from "../../components/pagiantion";
import { IForm } from "./type";
import { Link } from "react-router-dom";

const ProductForm:React.FC<IForm> = (props: IForm) => {
const {pathname} = useLocation()
    
    const data = props.data
    const isLoading =  props.isLoading
    const date:any = props.date
    
    return <div className="container">
    <div className="row">
        <div className="col-lg-12">
            <div className="main-box clearfix">
                <div className="table-responsive">
                    <Button head={props.head} link={props.link} text={props.text}/>                  
                    <table className="table user-list">
                        <thead>
                            <tr>                               
                                {
                                    props.thead.map((item,index) => {                                                                        
                                       return <th key={index}><span>{item}</span></th>
                                    })
                                }                               
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
						!props.isLoading && props.data ?  date.map((item: any,ind:number,dates:[])=>{
                            
                            if(pathname == "/product-list" || pathname =="/"){
                                return <tr key={item.id}>
							
							
							<td>
								<img src={item.image} alt="" />
								<a href="#" className="user-link">{item.name}</a>
								<span className="">{item.description}</span>
							</td>
							<td>
								<img src={item.brand.image} alt="" />
							</td>
							<td>
								${item.price}
							</td>
							<td>
								<span className="label label-default">{item.stock}</span>
							</td>
							
							<td style={{width: "20%" }}>
								<button className="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit">
									<Link to={`/update-product/${item.id}`} className="link">
									<i className="fa fa-edit">
								
									</i></Link></button>
								<button className="btn btn-danger btn-sm rounded-0" onClick={()=>{
									props.delete(item.id)
								}} type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i className="fa fa-trash"></i></button>
							</td>
						</tr>
                            }else{
                            
                           return <tr key={item?.id}>
                                 
                                      <React.Fragment key={item?.id}> 
                                     <td>
                                        {item?.id}
                                    </td>
                                        <td>
                                        {item?.name}
                                    </td>      
                                    <td>
                                    <img src={item?.image} alt=""/>                       
                                    </td>                        
                                    </React.Fragment>
                                                         
                                <td style={{width: "20%" }}>
								<button className="btn btn-danger btn-sm rounded-0" onClick={()=>{
									props.delete(item.id)
								}} type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i className="fa fa-trash"></i></button>
							</td>
                            </tr>
                            }
                             }) :   <Loading/>
                            
        
                     }
                        
                        </tbody>
                    </table>
                    <PaginatedItems total={props.total} isLoading={isLoading} data={data} onChange={props.onChange} />
                </div>
            </div>
        </div>
    </div>
    </div>
}

export default ProductForm
