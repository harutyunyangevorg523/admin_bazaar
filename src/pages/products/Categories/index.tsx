import React from "react"; 
import { useDeleteCategoryMutation, useGetCategoriesQuery } from "../../../redux/Creator/categoriesCreator";
import ProductForm from "../../../components/product_form";

const Categories:React.FC = () => {
    const [page, setPage] = React.useState(0);
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {		
		setPage((value * 5)-5);	
	};
	const [delete_category] = useDeleteCategoryMutation()	
      const { data, isLoading } = useGetCategoriesQuery({
        limit: 5,
        skip:page
    })
    return <ProductForm data={data} head="Categories List" link={"/create-category"} text={'Add Cateories'} delete={delete_category} thead={["Id","Name"]} total={data?.total} date={data?.product} isLoading={isLoading} onChange={handleChange as ()=> void} />
}

export default Categories