import "./style.css"
import React, { useState } from "react";
import { useDeleteProductMutation, useGetProductsQuery } from "../../../redux/Creator/productsCreator";
import ProductForm from "../../../components/product_form";

const List:React.FC = ()  => {

	
	
    const [page, setPage] = useState(0);
	
	const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {	
		setPage((value * 5)-5);	
	  };
	
	  
    const { data, isLoading } = useGetProductsQuery({
		limit: 5,
		skip: page
	})
	const [deleteProduct] = useDeleteProductMutation()	
	
	
        return <ProductForm data={data} head="Product List" link={"/create-product"} text="Add Product" delete={deleteProduct} thead={["Name","Brands","Price","Stock"]} total={data?.total} date={data?.product} isLoading={isLoading} onChange={handleChange as ()=> void} />

    
}

export default List 