import React from "react";
import { brand_schema } from "../../../components/Formik_Schema/brand_schema";
import { useAddBrandMutation } from "../../../redux/Creator/brandsCreator";
import CreateForm from "../../../components/create_form";
const Create_Brand:React.FC = () => {

  const [add_brand ] = useAddBrandMutation()
 
  const initialValues={
    name:"",
    image:"",
  }

 
  
  const onSubmit = () =>{

  }
 

 
    return <>
    <CreateForm text={"Add New Brand"} schema={brand_schema} add={add_brand} onSubmit={onSubmit as ()=> void} button={"Add Brand"} initialValues={initialValues}/>
    {/* <div className="cr_product container">
    <div>
     <h3 className="title">Add New Brand</h3>
     <div className="new_product">
    <form className="needs-validation">
       <div className="row g-3">
         <div>
         <Inputs type={"text"} id={"name"} className={`block w-full rounded border py-1 px-2 ${formik.touched.name && formik.errors.name ? 'is-invalid' : 'is-valid'}`} 
              placeholder={"Name"} required={formik.errors.name} onChange={formik.handleChange as () =>void}
              touched={formik.touched.name} errors={formik.errors.name} onBlur={formik.handleBlur as ()=> void} value={formik.values.name} />
           
         </div>
         <div className="mb-3 mt-3" onChange={getBase64}>
           <Inputs type={"file"} id={"image"} required={formik.errors.image} onBlur={formik.handleBlur as ()=> void} onChange={formik.handleChange as () =>void} className={`${formik.touched.image && formik.errors.image ? 'is-invalid' : 'is-valid'}`} accept=".jpeg,.png,.jpg" />
           
       
         </div>
         <div className="col-12">
         <button className="btn btn-primary btn-lg btn-block" onClick={formik.handleSubmit as () => void} type="button">Add Brand</button>
         </div>
         

       </div>
         
       
     </form>
     </div>
     </div>
 </div> */}
 </>
}

export default Create_Brand