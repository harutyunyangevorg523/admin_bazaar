import React from "react";
import { category_schema } from "../../../components/Formik_Schema/category_schema";
import { useAddCategoryMutation } from "../../../redux/Creator/categoriesCreator";
import CreateForm from "../../../components/create_form";
const Create_Category:React.FC = () => {
    const [ add_category] = useAddCategoryMutation()
    const onSubmit= ()=>{
    }
    const initialValues={
      name:""
    }
    
    return <CreateForm text={"Add New Categories"} initialValues={initialValues} schema={category_schema} add={add_category} onSubmit={onSubmit as ()=> void} button={"Add Categories"}/>

}

export default Create_Category