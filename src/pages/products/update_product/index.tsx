import React, { useMemo, useState } from "react";
import { useLocation } from 'react-router-dom';
import { product_schema } from "../../../components/Formik_Schema/product_schema";
import { useGetBrandsQuery } from "../../../redux/Creator/brandsCreator";
import { useGetCategoriesQuery } from "../../../redux/Creator/categoriesCreator";
import {  useEditProductMutation, useGetOneProductsQuery } from "../../../redux/Creator/productsCreator";
import CreateForm from "../../../components/create_form";
const Update_Product:React.FC = () => {
  const [loading , setLoading]=useState(true)
    let location = useLocation();
    const id: number = useMemo(() =>{
      return +(location.pathname.slice(16,location.pathname.length))
    },[location])
    
    const { data: oneProduct , isLoading} = useGetOneProductsQuery(id)
    const [editProduct] = useEditProductMutation()
    
    const { data: brand_data }=  useGetBrandsQuery({
        limit:10,
        skip:0
      })
      const { data: category_data } = useGetCategoriesQuery({
        limit:10,
        skip:0
      })
      
     const brand_date:any = brand_data?.product
      const category_date = category_data?.product
      
     const onSubmit = () =>{}
      const initialValues = {
        name: "",
        description: "",
        image: "",
        price: 1,
        stock:1,
        brandId: 1,
        categoriesId: 1
      }    
    return <CreateForm text={"Update Product"} loading={loading} id={id} oneProduct={oneProduct} isLoading={isLoading} category_data={category_date} brand_data={brand_date} editProduct={editProduct} initialValues={initialValues} schema={product_schema} onSubmit={onSubmit as ()=> void} button={"Update Product"}/>
}

export default Update_Product