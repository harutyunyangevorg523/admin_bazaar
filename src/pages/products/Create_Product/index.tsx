import "./style.css"
import 'react-toastify/dist/ReactToastify.css';
import React, { useMemo, useState} from "react";
import { useAddProductMutation } from "../../../redux/Creator/productsCreator";
import { product_schema } from "../../../components/Formik_Schema/product_schema";
import { useGetBrandsQuery } from "../../../redux/Creator/brandsCreator";
import { useGetCategoriesQuery } from "../../../redux/Creator/categoriesCreator";
import CreateForm from "../../../components/create_form";


const Create_Product:React.FC = () => {

  const [add_product ] = useAddProductMutation()
  const onSubmit = ()=>{

  }
  const initialValues = {
    name: "",
    description: "",
    image: "",
    price: 1,
    stock:1,
    brandId:1,
    categoriesId: 1
  }
       
    const { data: brand_data }=  useGetBrandsQuery({
      limit:10,
      skip:0
    })
    const { data: category_data } = useGetCategoriesQuery({
      limit:10,
      skip:0
    })
    
   const brand_date = brand_data?.product
    const category_date = category_data?.product
    
   
    
    return <CreateForm text={"Add New Product"} category_data={category_date} brand_data={brand_date} initialValues={initialValues} schema={product_schema} add={add_product} onSubmit={onSubmit as ()=> void} button={"Add Product"}/>
}

export default Create_Product
            