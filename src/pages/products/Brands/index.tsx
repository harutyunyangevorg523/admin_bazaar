import React from "react";
import { useDeleteBrandMutation, useGetBrandsQuery } from "../../../redux/Creator/brandsCreator"; 
import ProductForm from "../../../components/product_form";
const Brands:React.FC = () => { 
    const [page, setPage] = React.useState(0);
	const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {		
		setPage((value * 5)-5);	
	  };
      const { data, isLoading } = useGetBrandsQuery({
        limit:5,
        skip:page
    })
   
    const [ deleteBrand] = useDeleteBrandMutation()  
    
    
    
    return <ProductForm data={data} head="Brands List" link={"/create-brand"} text={'Add Brand'} delete={deleteBrand} thead={["Id","Name","Image"]} total={data?.total} date={data?.product} isLoading={isLoading} onChange={handleChange as ()=> void} />
}

export default Brands