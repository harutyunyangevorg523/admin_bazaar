import React, { useEffect, useState } from "react";


const useWindowsSize = () => {

    const [height, setHeight] = useState<any>(window.innerHeight)
    useEffect(() =>{
        window.addEventListener<any>('resize', function(event) {
           setHeight(event.currentTarget.innerHeight)
        }, true);
        return () => window.removeEventListener("resize",()=>{})
    },[])

   return {
    width: 0,
    height: height - 85
   }
}

export default useWindowsSize