import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.css"
import {  Routes, Route } from "react-router-dom";
import Create_Product from "../src/pages/products/Create_Product"
import Header from './components/header';
import Left_Bar from './components/header/Left-Bar';
import List from './pages/products/List';
import Categories from './pages/products/Categories';
import Brands from './pages/products/Brands';
import Create_Brand from './pages/products/Create_brand';
import Create_Category from './pages/products/Create_category/indx';
import Update_Product from './pages/products/update_product';
function App() {
  return ( <>
  <div className='flex'>
     <Left_Bar/>
     <div  className='max_width page_color'>
      <Header/>
      <Routes>
      <Route path='/' element={<List/>}/>
      <Route path='/product-list' element={<List/>}/>
      <Route path='/create-product' element={<Create_Product/>}/>
      <Route path='/category' element={<Categories/>}/>
      <Route path='/brands' element={<Brands/>}/>
      <Route path='/create-brand' element={<Create_Brand/>}/>
      <Route path='/create-category' element={<Create_Category/>}/>
      <Route path='/update-product/:id' element={<Update_Product/>}/>
    </Routes>
    </div>
    </div>
    </>
  );
}

export default App;
