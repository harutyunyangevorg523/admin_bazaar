import { createSlice, PayloadAction } from '@reduxjs/toolkit'


interface IClick {
    click: boolean
}

const initialState: IClick = {
    click: false,
}

export const menuReducer = createSlice({
    name: 'click',
    initialState,
    reducers: {
        changePar: (state,action: PayloadAction<boolean>) => {
            state.click = action.payload
        },

    },
})

export const { changePar } = menuReducer.actions


export default menuReducer.reducer