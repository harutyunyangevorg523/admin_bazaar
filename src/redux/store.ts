import {  combineReducers, configureStore  } from '@reduxjs/toolkit'
import clickReducer from "./reducers/menuReducer"
import { productsApi } from './Creator/productsCreator' 
import { setupListeners } from '@reduxjs/toolkit/dist/query'
import { brandsApi } from './Creator/brandsCreator'
import { categoriesApi } from './Creator/categoriesCreator'

const reducer =combineReducers({
  click:clickReducer,
  [productsApi.reducerPath]: productsApi.reducer,
  [brandsApi.reducerPath] : brandsApi.reducer,
  [categoriesApi.reducerPath] :  categoriesApi.reducer,
})
export const store = configureStore({
  reducer: reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(productsApi.middleware,brandsApi.middleware,categoriesApi.middleware)
}) 

setupListeners(store.dispatch)
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
