import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IAddCategory } from '../types/addCategory';
import { ICategories, IDeleteCategory } from '../types/categories';
interface IProps{
  limit:number,
  skip:number 
}
// Define a service using a base URL and expected endpoints
export const categoriesApi = createApi({
  reducerPath: 'categoriesApi',
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_BASE_URL }),
  endpoints: (builder) => ({
    
    getCategories: builder.query<ICategories, IProps>({
      query: ({limit,skip}) => `categories/?limit=${limit}&offset=${skip}`,
    }),
    addCategory: builder.mutation<IAddCategory,IAddCategory>({
      query: (payload) => ({
          url: '/categories',
          method: 'POST',
          body: payload,
        }),
        }),
        deleteCategory: builder.mutation<number,IDeleteCategory>({
          query: (id) => ({
            url: `/categories/${id}`,
            method: 'DELETE',
          }),
        })
  }),
  
})

export const { useGetCategoriesQuery , useAddCategoryMutation , useDeleteCategoryMutation} = categoriesApi