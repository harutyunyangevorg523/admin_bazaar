

import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type { IDeleteProduct, IProducts, IAddproducts , IOneProducts } from '../types/product'

interface IProps{
  limit:number,
  skip:number
}
interface IEdit{
  id:number
  payload?:IAddproducts,
}
export const productsApi = createApi({
  reducerPath: 'productsApi',
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_BASE_URL , headers: {
    'Content-type': 'application/json' }}),
  endpoints: (builder) => ({
    
    getProducts: builder.query<IProducts, IProps>({
      query: ({limit,skip}) => `products/?limit=${limit}&offset=${skip}`,
    }),
    addProduct: builder.mutation<IAddproducts,IAddproducts>({
      query: (payload) => ({
          url: '/products',
          method: 'POST',
          body: payload,
        }),
        }),
        deleteProduct: builder.mutation<number,IDeleteProduct>({
          query: (id) => ({
            url: `/products/${id}`,
            method: 'DELETE',
          }),
        }),
        getOneProducts: builder.query<IOneProducts, number>({
          query: (id) => `products/${id}`,
        }),
        editProduct: builder.mutation<IOneProducts,IEdit>({
          query: ({payload,id}) => ({
              url: `/products/${id}`,
              method: 'PUT',
              body:payload
            }),
            }),
  }),
  
})

export const { useGetProductsQuery , useAddProductMutation , useDeleteProductMutation , useGetOneProductsQuery , useEditProductMutation} = productsApi