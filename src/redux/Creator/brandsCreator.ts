import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IAddBrand } from '../types/addBrand';
import type { IBrands, IDeleteBrand } from '../types/brands'
interface IProps{
  limit?:number,
  skip?:number
}
// Define a service using a base URL and expected endpoints
export const brandsApi = createApi({
  reducerPath: 'brandsApi',
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_BASE_URL }),
  endpoints: (builder) => ({
    
    getBrands: builder.query<IBrands, IProps>({
      query: ({limit,skip}) => `brands/?limit=${limit}&offset=${skip}`,
    }),
    addBrand: builder.mutation<IAddBrand,IAddBrand>({
      query: (payload) => ({
          url: '/brands',
          method: 'POST',
          body: payload,
        }),
        }),
        deleteBrand: builder.mutation<number,IDeleteBrand>({
          query: (id) => ({
            url: `/brands/${id}`,
            method: 'DELETE',
          }),
        })
  }),
  
})

export const { useGetBrandsQuery , useAddBrandMutation , useDeleteBrandMutation } = brandsApi