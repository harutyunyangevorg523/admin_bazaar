export enum IBrands {
    GET_BRANDS = "GET_BRANDS"
}

export enum IAddBrand{
    ADD_BRAND = 'ADD_BRAND'
}