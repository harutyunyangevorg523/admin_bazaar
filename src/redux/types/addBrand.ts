
export interface IAddBrand{
    name?: string,
    image?: string
}

export type AddBrandState = {
    add_brand: IAddBrand[]
  }
  
  export type AddBrandAction = {
    type: string
    payload: IAddBrand[]
  }
  
 export  type DispatchType = (args: AddBrandAction) => AddBrandAction