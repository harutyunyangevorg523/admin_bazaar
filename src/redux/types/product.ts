import { IBrands } from "./brands"
import { ICategories } from "./categories"
export interface IProducts {
    product:{
      name: string,
    description: string,
    price: number,
    stock:number,
    image: string,
    categories:ICategories,
    brand:IBrands
    }
    total:number
}

export interface IAddproducts {
  name?: string,
  description?: string,
  price?: number,
  stock?:number,
  image?: string,
  brand?: number,
  categoriesIds?:number[],
}

export interface IOneProducts {
  name?: string
description?: string,
price?: number,
stock?:number,
image: string,
categories?:IOneCategories,
brand?:IOneBrands

}
export interface IOneCategories {
  
  0:{
   id?: number,
   name?: string,
   createdAt?: string,
   updatedAt?: string
  }
}
export interface IOneBrands {
  
    id?: number,
  name?: string,
  image: string,
  createdAt?: string,
  updatedAt?: string
  
}

export interface IDeleteProduct{
  id?:number
}
export type ProductsState = {
    product: IProducts[]
  }
  
  export type ProductsAction = {
    type: string
    payload: IProducts[]
  }
  
 export  type DispatchType = (args: ProductsAction) => ProductsAction