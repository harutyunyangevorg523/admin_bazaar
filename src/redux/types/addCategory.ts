
export interface IAddCategory{
    name?: string
}

export type AddCategoryState = {
    add_category: IAddCategory[]
  }
  
  export type AddCategoryAction = {
    type: string
    payload: IAddCategory[]
  }
  
 export  type DispatchType = (args: AddCategoryAction) => AddCategoryAction