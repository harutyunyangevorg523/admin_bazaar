export interface ICategories {
   product:{

    id: number,
    name: string
   }
   total:number
}
export interface IDeleteCategory{
  id?:number
}
export type CategoriesState = {
    categories: ICategories[]
  }
  
  export type CategoriesAction = {
    type: string
    payload: ICategories[]
  }
  
 export  type DispatchType = (args: CategoriesAction) => CategoriesAction