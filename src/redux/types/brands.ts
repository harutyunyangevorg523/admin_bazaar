export interface IBrands {
    product:{
      id?: number,
    name?: string,
    image?: string,
    createdAt?: string,
    updatedAt?: string
    }
    total:number
}
export interface IDeleteBrand{
  id?:number
}
export type BrandsState = {
    brands: IBrands[]
  }
  
  export type BrandsAction = {
    type: string
    payload: IBrands[]
  }
  
 export  type DispatchType = (args: BrandsAction) => BrandsAction